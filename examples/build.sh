#!/bin/bash
echo "[build.sh] Generating example boot animations!"
mkdir -p ../public
for folder in */; do
 cd $folder
 ./create.sh && echo "[build.sh] $folder animation created!" || echo "[build.sh] $folder animation failed!"
 cd ..
done