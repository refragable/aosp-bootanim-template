This media is based on works licensed under the terms of Creative Commons Attribution 4.0 International License ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)).

The original artist is [David Revoy](https://www.davidrevoy.com/) and all original media can be found at the [Pepper&Carrot website](https://www.peppercarrot.com/).
