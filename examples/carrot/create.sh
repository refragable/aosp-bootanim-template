#!/bin/bash
example="carrot"
echo "[create.sh] Creating $example boot animation now!"
zip -r bootanimation.zip info.txt COPYING.md part*/
cp bootanimation.zip ../../public/$example.zip