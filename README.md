# Android Boot Animation Template

This template was created with the Pixel 3 in mind. Be sure to update the files accordingly for your device. The TEMPLATE and all code created by me is hereby licensed under GPL v3.0 or later. Example bootanimations themselves are released under their own licenses since I may not own the artwork.

- [Android Boot Animation Template](#android-boot-animation-template)
  - [Structure of info.txt](#structure-of-infotxt)
  - [Image Preparation](#image-preparation)
  - [Creating a usable animation](#creating-a-usable-animation)
  - [Using with RattlesnakeOS](#using-with-rattlesnakeos)
  - [Example Bootanimations](#example-bootanimations)
    - [Carrot from Pepper&Carrot](#carrot-from-peppercarrot)

## Structure of info.txt

```
# Any extra space will be filled with BLACK 
# Width, Height, FPS
1080 2160 30
# pointer indicator, iterations to be played (ZERO = inf),
# interval in secs between each individual image, directory for the part
# the following plays part0 once, immediately switches to part1
# which will then loop infinitely until boot is completed.
p 1 0 part0
p 0 0 part1
```
## Image Preparation

The images used in the animation must come in `.jpg` or `.png` format in order to work.  
An example script using `jpegoptim` and `optipng` to optimize filesizes can be found [here](https://gitlab.com/refragable/shell-scripts/raw/master/convenience/image-optim.sh).


## Creating a usable animation

1. Prepare the animation images into the appropriate folders (e.g. `./part0/`, etc).
2. Edit `info.txt` with the timings and folder names
3. Create a zip file using: `zip -r bootanimation.zip <file1> <file2> <dir1> <etc>`
   - Or use the provided `create.sh` file, it will automatically use the files in the `template/` folder.
4. Flash `bootanimation.zip` on your device.

## Using with RattlesnakeOS

To add a custom boot animation you will need to edit your `.rattlesnake.toml` configuration customizations and maintain a [public git repository](https://gitlab.com/refragable/example_patch_shellscript) containing the patch script and boot animation zip file.

## Example Bootanimations

### Carrot from Pepper&Carrot

Animation featuring Carrot from the [Pepper&Carrot](https://www.peppercarrot.com) webcomic.  
This boot animation is licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/) and the art was originally made by David Revoy.